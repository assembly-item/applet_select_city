import {
  CityList
} from '../../components/wx-city-picker/pca.js';
Page({
  data: {
    codes: [130000, 130200, 130203],
    city: '',
    citylist: CityList
  },
  onLoad() {},
  onSelect(e) {
    console.log(e)
    this.setData({
      codes: e.detail.code,
      city: e.detail.value
    })
  }
})