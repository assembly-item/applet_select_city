# 小程序选择城市

#### 介绍
小程序选择城市组件 picker-view 一次性获取所有数据、体验较好

#### 软件架构
1. components/wx-city-picker:选择城市组件
- pc.js省市数据 / pca.js省市区数据
2. pages/index：组件使用


#### 使用说明

1.  codes默认地区id组

#### 配置
##### 参数

 选项名      | 类型    |  是否必填  |  默认值  | 描述 
 --------    | -----:  | :----: | :----: | :----: 
mode        | Number      |   false    |   1    |  选择器类型：1-省市区选择，2-省市选择    |
 codes       | Array      |   false    |   [0, 0, 0]  | 省市(区)的编码数组，如：[110000, 110100, 110101]
 data        | Array      |   true    |       | 省市区数据，省市区数据示例:pca.js ,省市数据示例:pc.js
 childkey        | String      |   false    |  ‘children’   | 数据中子数组键名
 idkey        | String      |   false    |    ‘id’   | 省市区编码键名
 namekey        | String      |   false    |   ‘name’    | 省市区名称键名


##### 事件

 选项名      | 类型    |  是否必填    | 描述 
 --------    | -----:  | :----: | :----:
 select        | Function      |   true    |  返回选中省市(区)信息



#### tips
1. 两个版本 
- 1、点击弹出选择城市框(第一次提交版本) 2、直接在页面展示城市选择框（最新版本）
